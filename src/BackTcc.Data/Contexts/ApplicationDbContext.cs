using BackTcc.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BackTcc.Data
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<Cargo> Cargo { get; set; }
        public DbSet<Certificacoes> Certificacoes { get; set; }
        public DbSet<Contatos> Contatos { get; set; }
        public DbSet<Empresa> Empresa { get; set; }
        public DbSet<ExperienciaProfissional> ExperienciaProfissional { get; set; }
        public DbSet<FormacaoAcademica> FormacaoAcademica { get; set; }
        public DbSet<Habilidades> Habilidades { get; set; }
        public DbSet<Pessoa> Pessoa { get; set; }
        public DbSet<PessoaHabilidadeRecomendacoes> PessoaHabilidadeRecomendacoes { get; set; }
        public DbSet<PessoaHabilidades> PessoaHabilidades { get; set; }
        public DbSet<PessoaInteresses> PessoaInteresses { get; set; }
        public DbSet<Projeto> Projeto { get; set; }
        public DbSet<ProjetoHabilidades> ProjetoHabilidades { get; set; }
        public DbSet<ProjetoParticipantes> ProjetoParticipantes { get; set; }
    }
}
