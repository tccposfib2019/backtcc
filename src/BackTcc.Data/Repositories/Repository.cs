using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BackTcc.Domain;

namespace BackTcc.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly ApplicationDbContext _context;

        public Repository (ApplicationDbContext context) 
        {
            _context = context;            
        }

        public void Create (TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            _context.SaveChanges();
        }

        public void Update (TEntity entity)
        {
            entity.UpdatedAt = DateTime.Now;
            _context.Update(entity);
            _context.SaveChanges();
        }

        public void Delete (long id)
        {
            var entity = this.GetById(id);
            entity.UpdatedAt = DateTime.Now;
            entity.Status = (byte)StatusEnum.Deleted;
            _context.Update(entity);
            _context.SaveChanges();
        }

        public TEntity GetById(long id)
        {
            return _context.Set<TEntity>().FirstOrDefault(x => x.Id == id);
        }

        // public Project GetByName(string name)
        // {
        //     return _context.Set<Project>().FirstOrDefault(x => x.Name.Equals(name));
        // }

        public IList<TEntity> Get()
        {
            return _context.Set<TEntity>().Where(x => x.Status.Equals(1)).ToList();
        }
    }
}    
