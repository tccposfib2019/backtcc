using System.Collections.Generic;

namespace BackTcc.Domain
{
    public interface IRepository<TEntity>
    {
        void Create(TEntity entity);
        
        void Update(TEntity entity);

        void Delete (long id);
        
        TEntity GetById(long id);
        
        IList<TEntity> Get();
    }
}
