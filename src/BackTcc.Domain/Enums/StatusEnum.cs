using System;

namespace BackTcc.Domain
{
    public enum StatusEnum
    {
        Active = 1,
        Deleted = 2
    }
}
