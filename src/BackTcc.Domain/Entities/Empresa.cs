namespace BackTcc.Domain.Entities
{
    public class Empresa : Entity
    {
        public string Nome { get; set; }
        public string AreaAtuacao { get; set; }
        public string Descricao { get; set; }
    }
}