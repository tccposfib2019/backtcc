namespace BackTcc.Domain.Entities
{
    public class Habilidades : Entity
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
    }
}