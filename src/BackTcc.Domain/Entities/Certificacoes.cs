using System;

namespace BackTcc.Domain.Entities
{
    public class Certificacoes : Entity
    {
        public string Titulo { get; set; }
        public string Orgao { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }

    }
}