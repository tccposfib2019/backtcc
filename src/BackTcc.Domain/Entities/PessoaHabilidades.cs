namespace BackTcc.Domain.Entities
{
    public class PessoaHabilidades
    {
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public int HabilidadesId { get; set; }
        public virtual Habilidades Habilidades { get; set; }
    }
}