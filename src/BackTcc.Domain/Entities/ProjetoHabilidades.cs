namespace BackTcc.Domain.Entities
{
    public class ProjetoHabilidades
    {
        public int ProjetoId { get; set; }
        public virtual Projeto Projeto { get; set; }
        public int HabilidadesId { get; set; }
        public virtual Habilidades Habilidades { get; set; }
    }
}