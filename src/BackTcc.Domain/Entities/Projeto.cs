using System;

namespace BackTcc.Domain.Entities
{
    public class Projeto : Entity
    {
        public int EmpresaId { get; set; }
        public virtual Empresa Empresa { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public DateTime DateInicio { get; set; }
        public DateTime DateFim { get; set; }
    }
}