namespace BackTcc.Domain.Entities
{
    public class Contatos : Entity
    {
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public string TipoContato { get; set; }
        public string Contato { get; set; }
    }
}