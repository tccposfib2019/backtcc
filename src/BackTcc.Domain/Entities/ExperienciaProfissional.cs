using System;

namespace BackTcc.Domain.Entities
{
    public class ExperienciaProfissional : Entity
    {
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public int CargoId { get; set; }
        public virtual Cargo Cargo { get; set; }
        public int EmpresaId { get; set; }
        public virtual Empresa Empresa { get; set; }
        public string Localidade { get; set; }
        public DateTime DateInicio { get; set; }
        public DateTime DateFim { get; set; }
        public string Titulo { get; set; }
        public string DescricaoAtividade { get; set; }
    }
}