namespace BackTcc.Domain.Entities
{
    public class Cargo : Entity
    {
        public string Nome { get; set; }

        public string Descricao { get; set; }
    }
}