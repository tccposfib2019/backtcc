namespace BackTcc.Domain.Entities
{
    public class FormacaoAcademica
    {
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public string Instituicao { get; set; }
        public string Formacao { get; set; }
        public string AreaEstudo { get; set; }
        public int AnoInicio { get; set; }
        public int AnoConclusao { get; set; }
    }
}