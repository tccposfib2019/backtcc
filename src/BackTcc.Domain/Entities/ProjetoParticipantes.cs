using System;

namespace BackTcc.Domain.Entities
{
    public class ProjetoParticipantes : Entity
    {
        public int ProjetoId { get; set; }
        public virtual Projeto Projeto { get; set; }
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public string Funcao { get; set; }
        public string Feedback { get; set; }
        public DateTime DateInicio { get; set; }
        public DateTime DateFim { get; set; }
    }
}