using System;

namespace BackTcc.Domain
{
    public class Entity
    {
        public int? Id { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public byte Status { get; set; } = 1;
    }
}
