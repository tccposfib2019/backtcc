using System;

namespace BackTcc.Domain.Entities
{
    public class PessoaHabilidadeRecomendacoes : Entity
    {
        public int PessoaId { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public int HabilidadesId { get; set; }
        public virtual Habilidades Habilidades { get; set; }
        public string Recomendacao { get; set; }
        public string PessoaRecomendacao { get; set; }
        public DateTime DateRecomendacao { get; set; }
    }
}