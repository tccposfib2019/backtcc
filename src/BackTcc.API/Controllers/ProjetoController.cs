using System;
using System.Collections.Generic;
using BackTcc.Domain;
using BackTcc.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;


namespace BackTcc.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjetoController : ControllerBase
    {
        private readonly IRepository<Projeto> _projetoRepository;
        public ProjetoController(IRepository<Projeto> projetoRepository)
        {
            _projetoRepository = projetoRepository;
        }

        [HttpGet]
        public IEnumerable<Projeto> Get()
        {
            return _projetoRepository.Get();
        }

        [HttpGet("{id}")]
        public Projeto Get(long id)
        {
            return _projetoRepository.GetById(id);
        }

        [HttpPost]
        public void Create(Projeto model)
        {
            _projetoRepository.Create(model);
        }

        [HttpPut("{id}")]
        public void Put(int id, Projeto model)
        {
            _projetoRepository.Update(model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _projetoRepository.Delete(id);
        }

    }
}